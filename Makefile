all: funcion1_escalar
#all: funcion2_escalar
#all: funcion1_vectorial
#all: funcion2_vectorial

funcion1_escalar: funcion1_escalar.c
#funcion1_vectorial: funcion1_vectorial.c
#funcion2_escalar: funcion2_escalar.c
#funcion2_vectorial: funcion2_vectorial.c

	gcc -std=c99 funcion1_escalar.c -lm -o exec
	#gcc -std=c99 funcion1_vectorial.c -lm -o exec -msse -msse2 -msse3
	#gcc -std=c99 funcion2_escalar.c -lm -o exec
	#gcc -std=c99 funcion2_vectorial.c -lm -o exec -msse -msse2 -msse3

run:	
	time ./exec -f entrada.in