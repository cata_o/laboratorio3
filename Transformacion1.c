#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 100000
char nombre[] = "/";
int largo;

float calcular(float *a, float *b){
    int j;
    float aux[N] __attribute__((aligned(16)));
    float acc = 0;
    float acc2 = 0;
    __m128 v;
    __m128 v2;
    __m128 aux1;
    __m128 aux2;
    for (size_t i = 0; i < largo; ++i) {
        aux[i] = 2;
    }
    v = _mm_load_ps(a); //ccontiene el valor de a
    v2 = _mm_load_ps(b); // contiene el valor de b
    aux1 = _mm_load_ps(aux); // contiene un vector solo con valores 2
    aux1 = _mm_div_ps(v,aux1); //contiene la div de v/2
    aux2 = _mm_add_ps(aux1,_mm_sqrt_ps(v2)); //contiene la suma 
    _mm_store_ps(aux,aux2);
    for (j = 0; j < largo; j++){  // se realiza la sumatoria
        acc = acc + aux2[j];
    }
    for (j = 0; j < largo; ++j) { // se realiza el llenado de un vector solo con el valor del contenido anterior
        aux [j] = acc;
    }
    aux1 = _mm_load_ps(aux);
    aux2 = _mm_sub_ps(_mm_mul_ps(v,v),aux1);
    _mm_store_ps(aux,aux2);    
    for (j =0; j<largo; j++){  // se realiza la sumatoria
        acc2 = acc2 + aux2[j];
    }
    printf("Resultado:  %f\n", acc2);
    return acc2;
}

int main(int argc, char ** argv){
    float a[N] __attribute__((aligned(16)));
    float b[N] __attribute__((aligned(16)));
    float dato;
    int i = 0, opcion;
    while ((opcion = getopt (argc, argv, "f: ")) != -1){
        switch (opcion){
            case 'f':
                strcpy(nombre, optarg);
                FILE* archivo = fopen (nombre, "r");
                if (archivo == NULL){
                    perror("ERROR");
                }
                else {
                    while(!feof(archivo)){
                        fscanf(archivo,"%f", &dato);
                        a[i] = dato;
                        b[i] = dato;
                        i++;
                    }
                    largo = i;
                    printf("%f\n", calcular(a, b));
                }
        }
    }
    return 0;
}