#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>

#include <stdio.h>
#include <malloc.h>
#include <stdio.h>

#define N 100000

int largo;

int calcular(int *a, int *b){
int acc = 0, i=0;
    
    int aux[N] __attribute__((aligned(16)));
    __m128 v;
    __m128 v2;
    v = _mm_load_ps(a);
    v2 = _mm_load_ps(b);
    v = _mm_xor_ps(v,v2);
    _mm_store_ps(aux,v);
    for(i = 0; i < largo - 1; i++)
    {   
       acc = acc + aux[i];
    }
    return acc;
}

int main(int argc, char *argv[])
{
    int dato;
    char nombre[]="entrada.txt";
    int i = 0;
    int a[N] __attribute__((aligned(16)));
    int b[N] __attribute__((aligned(16))); 
    FILE* archivo = fopen (nombre, "r");
    while(!feof(archivo)){
        fscanf(archivo,"%f", &dato);
        printf("%f\n",dato );
        a[i]=dato;
        i++;
    }
    largo = i;
    for(i=0; i<largo-1; i++){
        b[i]=a[i+1];
    }
    printf("%d\n", calcular(a,b));
    
    return 0;
}
/*
int calcular(char *a){
    int acc = 0;

    for(size_t i = 0; i < N - 1; i++)
    {
       char tmp = a[i] ^ a[i + 1];
       acc = acc + tmp;
    }

    return acc;
}*/