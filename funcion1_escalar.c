#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define N 100000
char nombre[] = "/";
/*
QUE REALIZA LA FUNCIÓN: la función obtiene los valores desde el archivo de entrada, y lo pasa a un vector, 
                        para luego realizar todos los calculos necesarios y meterlos a otro vector, para luego imprimir por pantalla el resultado.
ENTRADA: VOID
SALIDA: VOID

*/

void leer(){
	float dato, vector [N], acumulador;
	int i, largo;
	i = 0;
	FILE* archivo = fopen (nombre, "r"); //Acá se abre el archivo en modo lectura
	if (archivo == NULL){
        perror("ERROR");
    }
    else {
		while(!feof(archivo)){ //Este es el ciclo que lee todos los datos en el archivo de entrada, lee hasta que se encuentra con el final del archivo.
			fscanf(archivo, "%f", &dato); //Acá le pasamos el formato en el que debe leer, en este caso, un número por cada linea.
			vector[i] = dato; //Ingresamos el valor leido a un vector inicial
			i++;
		}
		largo = i;
		float vector1[largo], vector2[largo]; //Se crean dos vectores auxiliares para realizar los calculos
		for(i = 0; i < largo; i++){
			vector1[i] = pow(vector[i] ,vector[i]); //Realizamos el primer cálculo, que es la potencia del numero elevado al mismo numero
			vector2[i] = sqrt(vector1[i]); //Luego se obtiene la raiz del numero obtenido en el primer cálculo.
			acumulador = vector2[i] + acumulador; //Acumulamos los valores calculados y guardados en los vectores auxiliares
		}
		printf("El resultado final es %f\n", acumulador);//Finalmente se imprime el valor obtenido por la salida estandar.
		fclose(archivo);
	}
}

int main(int argc, char ** argv){
	int opcion;
	while ((opcion = getopt (argc, argv, "f: ")) != -1){
		switch (opcion){
			case 'f':
				strcpy(nombre, optarg);
				leer();
				break;
		}
	}
	return 0;
}