#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define N 100000
char nombre[] = "/";


int main(int argc, char ** argv){
    float dato, aux, acum=0 ;
    int largo, j=0, opcion;
    float vector[N] __attribute__((aligned(16))); //Se crea el vector para guardar los valores leidos desde el archivo de entrada, alienado en 16 bits, como se explico en la ayudantía.
    while((opcion = getopt (argc, argv, "f: ")) != -1){
        switch (opcion){
            case 'f':
                    strcpy(nombre, optarg);                    
                    FILE* archivo = fopen (nombre, "r");
                    if (archivo == NULL){
                        perror("ERROR");
                    }
                    else {
                        while(!feof(archivo)){
                            fscanf(archivo,"%f", &dato);//Se lee el dato desde el archivo de entrada
                            vector[j]=dato; //se guadar el dato en el vector
                            j++;
                        }
                        largo = j;
                        __m128 v; //se crea el vector para guardar los datos
                        for(size_t i = 0; i < largo; i=i+1){
                            vector[i] = pow(vector[i],vector[i]); //Se recorre el vector y se calcula la potencia, guardando el resultado en el vector inicial
                        }
                        v = _mm_load_ps(vector);//luego se guardan los resultados en el vector v creado anteriormente.
                        v = _mm_sqrt_ps(v);
                        _mm_store_ps(vector, v);
                        for(j = 0; j<largo; j++){
                            acum = acum + vector[j];
                        }
                        printf("El valor final calculado es: %f\n", acum);//Acá se imprime el valor final de lo calculado
                        fclose(archivo);
                        break;
                    }
        }
    }
    return 0;
}