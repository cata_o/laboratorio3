#include <stdio.h>
#include <getopt.h>

#define N 100000
char nombre[] = "/";


void leer(){
	float dato, vector [N], vector2 [N], acumulador; //Declaración de todos los vectores necesarios para los cálculos dentro de la función
	int i, largo;
	acumulador = 0, i = 0; //Inicialización de las variables auxiliares en 0 para luego sumar dentro de ellas
	FILE* archivo = fopen (nombre, "r");//Apertura del archivo en modo lectura
	if (archivo == NULL){
        perror("ERROR");
    }
    else {
		while(!feof(archivo)){
			fscanf(archivo,"%f", &dato);//Lectura de los datos desde el archivo de entrada
			vector[i] = dato;//Ingreso de los valores al vector inicial
			i++;
		}
		largo = i;
		for(i = 0; i < largo-1; i++ ){
			vector2 [i] = vector [i] * vector [i+1];//Multiplicación de los numero del vector y su susesor
			acumulador = acumulador + vector2 [i];//Suma de todos los valores obtenidos.
			printf("vector[%d]:  %f %f\n",i, vector[i], vector2[i] );
 		}
 		printf("El valor final calculado es: %f\n", acumulador);//Retorno del valor final calculado
 		fclose(archivo);
 	}
}

int main(int argc, char ** argv){
	int opcion;
	while ((opcion = getopt (argc, argv, "f: ")) != -1){
		switch (opcion){
			case 'f':
					strcpy(nombre, optarg);
					leer();
					break;
		}
	}
	return 0;
}