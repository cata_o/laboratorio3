#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define N 100000
char nombre[] = "/";


int main(int argc, char *argv[]){
	float vector[N] __attribute__((aligned(16)));
	float vector1[N] __attribute__((aligned(16)));
	float dato, acumulador = 0;
	int j = 0, largo, opcion;
	while((opcion = getopt (argc, argv, "f: ")) != -1){
        switch (opcion){
            case 'f':
                    strcpy(nombre, optarg);
					FILE* archivo = fopen (nombre, "r");
					if (archivo == NULL){
                        perror("ERROR");
                    }
                    else {
						while(!feof(archivo)){
							fscanf(archivo,"%f", &dato);
							vector[j]=dato;
							j++;
						}
						largo = j;
						for(j = 0; j < largo-1; j++){
							vector1 [j] = vector [j+1];
						}
						__m128 v;
						__m128 v2;
    					v = _mm_load_ps(vector);
    					v2 = _mm_load_ps(vector1);
						v2 = _mm_mul_ps(v2, v);
 						_mm_store_ps(vector,v2);
						for(j = 0; j < largo-1; j++ ){
 							acumulador = acumulador + vector[j];
 						}
 						fclose(archivo);
 						printf("El valor final calculado: %f\n", acumulador);
 					}
 					break;
 		}
 	}
 	return 0;
}